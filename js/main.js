// Novo game
var game = new Phaser.Game(1024, 600, Phaser.AUTO, 'phaser-canvas');

game.state.add('credits', creditState);
game.state.add('menu', mainMenuState);
game.state.add('selectionScene', phaseSelectState);
game.state.add('game', gameState);
game.state.add('instructions', instructionState);
game.state.add('dead', deadState);
game.state.add('win', winState);
game.state.add('cutscene', cutsceneState);

// Iniciando o Menu Principal
game.state.start('menu');