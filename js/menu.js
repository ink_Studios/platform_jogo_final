"use strict";

//Função para controle do Menu Principal, com disposição dos botões "Play" e "Credits"
var background;
var music; 
var musicControl = true; /* variável para controlar se a música deve ser tocada novamente ou não, quando o jogador volta da tela Credits;   
                            Evita que uma outra instância da música seja criada */

var mainMenuState = {
    
    preload: function() { 
        this.game.load.image('background', 'assets/backgrounds/backgroundMenu.jpg'); // Plano de fundo
        this.game.load.image('gameTitle', 'assets/backgrounds/gameTitle.png');  //Título
        this.game.load.image('particle', 'assets/particles/menuParticle.png'); // Partículas
        this.game.load.spritesheet('playButton', 'assets/buttons/playButton.png', 188, 65); // Botão PLAY
        this.game.load.spritesheet('creditsButton', 'assets/buttons/creditsButton.png', 276, 65); // Botão CREDITS
        this.game.load.audio('audio', 'assets/audio/menu.ogg'); // Aúdio de fundo
    },  
     
    goToGame: function() {   //Função que redireciona para a seleção de fases
        music.mute = true;
        this.game.state.start('cutscene');
    },  
    
    goToCredits: function() {  // Função que redireciona para a tela de Créditos    
        musicControl = false;
        this.game.state.start('credits');
    },

    create: function() { 
    
        if(musicControl) {
            music = game.add.audio('audio', 1, true);
            music.play();
        }
        
        background = game.add.tileSprite(0, 0, 1024, 600, 'background');  // Adiciona o plano de fundo 
                
        // Adiciona os botões e as respectivas animações
        this.game.add.button(100, 400, 'playButton', this.goToGame.bind(this), this, 1, 2);  
        this.game.add.button(650, 400, 'creditsButton', this.goToCredits.bind(this), this, 1, 2);
        
        // Partículas 
        var emitter = game.add.emitter(game.world.centerX, 450, 200);
        emitter.makeParticles('particle');

        emitter.setRotation(0, 0);
        emitter.setAlpha(0.3, 0.8);
        emitter.setScale(0.5, 1);
        emitter.gravity = -200;
        emitter.start(false, 1500, 100);
        
        background = game.add.tileSprite(0, 0, 1024, 600, 'gameTitle'); // Adiciona Título
    }
}