"use strict";

//Função para controle da tela de Instruções, com disposição do botão "Back"
var background;
var transition;

var instructionState = {
    
    preload: function() {
        this.game.load.image('background', 'assets/backgrounds/backgroundInstructions.jpg');
        this.game.load.spritesheet('continueButton', 'assets/buttons/continueButton.png', 151, 33);
        this.game.load.spritesheet('transition', 'assets/transitions/phaseTransition.png', 1024, 600);
    },  
     
    goToGame: function() {
        this.game.state.start('selectionScene'); //Função que redireciona para o Menu Principal
    },  
    
    createScreen: function() {
        background = game.add.tileSprite(0, 0, 1024, 600, 'background');

        this.game.add.button(820, 500, 'continueButton', this.goToGame.bind(this), this, 1, 2);  
    },

    create: function() {  //A transição acontece aqui
        transition = game.add.sprite(0, 0, 'transition');
        transition.animations.add('go', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 2000, true);
        
        transition.animations.play('go'); 
                
        //a Tela de Seleção de fases é carregada aqui
        this.game.time.events.add(900, this.createScreen, this);  
    }
}