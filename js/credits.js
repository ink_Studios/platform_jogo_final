"use strict";

//Função para controle da tela Créditos, com disposição do botão "Back"

var background; // fundo da tela    
var transition; // variável de controle da transição entre as telas

var creditState = {
    
    preload: function() {
        this.game.load.image('background', 'assets/backgrounds/backgroundCredits.jpg'); // Plano de fundo
        this.game.load.spritesheet('backButton', 'assets/buttons/backButton.png', 145, 40); // Botão Voltar
        this.game.load.spritesheet('transition', 'assets/transitions/phaseTransition.png', 1024, 600); // Splash
    },  
     
    goToGame: function() {
        this.game.state.start('menu'); //Função que redireciona para o Menu Principal
    }, 
    
    createScreen: function() {
        background = game.add.tileSprite(0, 0, 1024, 600, 'background');
        this.game.add.button(100, 500, 'backButton', this.goToGame.bind(this), this, 1, 2);  // botão de voltar
    },

    create: function() { 
        transition = game.add.sprite(0, 0, 'transition');
        transition.animations.add('go', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 2000, true);
        
        transition.animations.play('go');
        
        this.game.time.events.add(900, this.createScreen, this);
    }
}