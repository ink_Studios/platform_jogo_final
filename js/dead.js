"use strict";

//Função para controle da tela Derrota, com disposição do botão "Exit" e "Replay"

var background; // fundo da tela    
var transition; // variável de controle da transição entre as telas

var deadState = {
    
    preload: function() { 
        this.game.load.image('background', 'assets/backgrounds/gameOverBackground.jpg'); // Plano de fundo
        this.game.load.spritesheet('exitButton', 'assets/buttons/exitButton.png', 120, 50);  // Botão Exit
        this.game.load.spritesheet('replayButton', 'assets/buttons/replayButton.png', 282, 52);  // Botão Replay
        this.game.load.spritesheet('transition', 'assets/transitions/phaseTransition.png', 1024, 600); // Splash
    },  
     
    goToGame: function(){
        this.game.state.start('game'); //Função que redireciona para o Jogo
    }, 
    
    goToMenu: function(){
        this.game.state.start('menu'); //Função que redireciona para o Menu Principal
    },
    
    createScreen: function(){
        background = game.add.tileSprite(0, 0, 1024, 600, 'background');
        this.game.add.button(360, 400, 'replayButton', this.goToGame.bind(this), this, 1, 2);  // botão de Replay
        this.game.add.button(450, 500, 'exitButton', this.goToMenu.bind(this), this, 1, 2);  // botão de Exit
    },

    create: function(){
        
        transition = game.add.sprite(0, 0, 'transition');
        transition.animations.add('go', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 2000, true);
        
        transition.animations.play('go');
        
        this.game.time.events.add(900, this.createScreen, this);
    }
}