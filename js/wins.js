"use strict";

//Função para controle da tela Cutscene, com disposição do botão "Skip"

var background; // plano de fundo da tela        
var transition; // transição entre telas
var song;       // som de fundo da tela
var able = false; //variável para permitir a exibição da cutscene em si

var winState = {
    
    preload: function() {
        this.game.load.image('victory', 'assets/backgrounds/backgroundVictory.jpg'); // Plano de fundo
        this.game.load.spritesheet('skipButton', 'assets/buttons/skipButton.png', 124, 48); // Botão de Skip
        this.game.load.spritesheet('transition', 'assets/transitions/phaseTransition.png', 1024, 600); // Splash
		this.game.load.audio('cutscene', 'assets/audio/cutscene.mp3'); // Audio 
    },  
     
    goToMenu: function() { 
		song.mute = true;  // Destroi a música que está tocando
        this.game.state.start('menu'); //Redireciona para a tela de Instruções
    },  
    
    createScreen: function() {
        song.fadeIn(20); // Inicia a música
        
        background = game.add.tileSprite(0, 0, 1024, 600, 'victory');
        this.game.add.button(820, 500, 'skipButton', this.goToMenu.bind(this), this, 1, 2);  
        
        able = true;
    },
    
    decodeSound: function() {
        song.onDecoded.add(this.createScreen, this);
    },

    create: function() { // Aqui a animação acontece
        song = game.add.audio('cutscene');
		
        transition = game.add.sprite(0, 0, 'transition');
        transition.animations.add('go', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 2000, true);
        
        transition.animations.play('go');
        
        song.fadeIn(20); // Inicia a música
        
        background = game.add.tileSprite(0, 0, 1024, 600, 'victory');
        this.game.add.button(820, 500, 'skipButton', this.goToMenu.bind(this), this, 1, 2);  
        console.log(background);
        able = true;
        
       // this.game.time.events.add(100, this.decodeSound, this);  
    },
    
    update: function() {
        if(able) {
            console.log(background);
            background.tilePosition.x -= 2.3;
            
            if(background.tilePosition.x <= -4096) { // checa se a cutscene já chegou ao fim e, caso tenha chegado, redireciona para o menu
                this.goToMenu();
            }
        } 
    } 
}