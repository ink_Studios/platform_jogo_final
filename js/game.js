"user strict"

var map;
var audio;
var tileset;
var layer;
var facing = 'left';
var jumpTimer = 0;
var cursors;
var jumpButton;
var bg;
var player;
var portal;

// Variáveis para controle de pause
var pause; 
var pauseBtn;

var bola1;
var	bola2;
var	bola3;
var	bola4;
var	bola5;
var	bola6;
var	bola7;

// First Phase State 
var gameState = {

    preload: function(){
        this.game.load.image('tiled1', 'assets/tiled1.jpg');  // Tiled
        this.game.load.image('pause', 'assets/backgrounds/pause.jpg'); // Pause
        this.game.load.image('background', 'assets/backgrounds/backgroundFase1.jpg'); // Plano de fundo
        this.game.load.spritesheet('dude', 'assets/player/player.png', 65, 84); // Personagem principal  
        this.game.load.tilemap('fase1', 'assets/fase1.json', null, Phaser.Tilemap.TILED_JSON); // Tilemap
        this.game.load.image('portal', 'assets/objects/porta.jpg');
        
        this.game.load.audio('audio', 'assets/audio/carousel.mp3'); // Áudio plano de fundo
            
        // Carregando a bola
        this.game.load.image('bola1', 'assets/balls/bola1.png');
        this.game.load.image('bola2', 'assets/balls/bola2.png');
        this.game.load.image('bola3', 'assets/balls/bola3.png');
        this.game.load.image('bola4', 'assets/balls/bola4.png');

        // Carregando o palhaço
        this.game.load.spritesheet('clown', 'assets/clown.png', 57, 107);
},

    create: function(){
        audio = game.add.audio('audio', 1, true);
        audio.play();
  
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        bg = game.add.tileSprite(0, 0, 2000, 600, 'background');
        bg.fixedToCamera = true;

        map = game.add.tilemap('fase1');
        map.addTilesetImage('tiled1');
        map.setCollisionByExclusion([ 13, 14, 15, 16, 46, 47, 48, 49, 50, 51 ]);

        layer = map.createLayer('Tile Layer 1');
        layer.resizeWorld();

        game.physics.arcade.gravity.y = 250;

        //Criando os inimigos do jogo
        //bolas = game.add.group();
        //bolas.enableBody = true;
        //bolas.physicsBodyType = Phaser.Physics.ARCADE;
        this.bola1 = this.game.add.sprite(200,200,"bola1");
        this.game.physics.enable(this.bola1);
        //this.bola1.enableBody = true;
        //this.bola1.body.immovable=true;
        this.bola1.body.gravity = 0;

        this.bola2 = this.game.add.sprite (5864,200,"bola2");
        this.game.physics.enable(this.bola2);
        this.bola2.body.gravity = 0;

        this.bola3 = this.game.add.sprite (6120,64,"bola3");
        this.game.physics.enable(this.bola3);
        this.bola3.body.gravity = 0;

        this.bola4 = this.game.add.sprite (8064,64,"bola4");
        this.game.physics.enable(this.bola4);
        this.bola4.body.gravity = 0;

        this.bola5 = this.game.add.sprite (11520,0,"bola3");
        this.game.physics.enable(this.bola5);
        this.bola5.body.gravity = 0;

        this.bola6 = this.game.add.sprite (11648,192,"bola1");
        this.game.physics.enable(this.bola6);
        this.bola6.body.gravity = 0;

        this.bola7 = this.game.add.sprite (11776,0,"bola4");
        this.game.physics.enable(this.bola7);
        this.bola7.body.gravity = 0;

        this.bola8 = this.game.add.sprite (1664,320,"bola4");
        this.game.physics.enable(this.bola8);
        this.bola8.body.gravity = 0;

        this.bola9 = this.game.add.sprite (2880,256,"bola2");
        this.game.physics.enable(this.bola9);
        this.bola9.body.gravity = 0;

        this.bola10 = this.game.add.sprite (5568,512,"bola3");
        this.game.physics.enable(this.bola10);
        this.bola10.body.gravity = 0;

        this.bola11 = this.game.add.sprite (7552,448,"bola1");
        this.game.physics.enable(this.bola11);
        this.bola11.body.gravity = 0;
        this.s11 = Math.random()

        this.bola12 = this.game.add.sprite (7744,320,"bola4");
        this.game.physics.enable(this.bola12);
        this.bola12.body.gravity = 0;
        this.s12 = Math.random()

        this.bola13 = this.game.add.sprite (7936,320,"bola3");
        this.game.physics.enable(this.bola13);
        this.bola13.body.gravity = 0;
        this.s13 = Math.random()

        this.bola14 = this.game.add.sprite (8128,448,"bola1");
        this.game.physics.enable(this.bola14);
        this.bola14.body.gravity = 0;
        this.s14 = Math.random();
        
        //CRIANDO OS PALHAÇOS.
        //clown1 = this.clowns.create(1600, 200, 'clown');
        this.clown1 = this.game.add.sprite (1600,200,"clown");
        this.game.physics.enable(this.clown1);
        this.game.physics.arcade.enable(this.clown1);
        this.clown1.enableBody = true;
        this.clown1.animations.add('walk', [0, 1, 2], 10, true);
        //this.clown1.animations.add('e', [4, 5, 6], 10, true);
        this.clown1.animations.add('idle', [2], 20, true);
        this.clown1.anchor.setTo (0.5,0.5);


        //clown2 = this.clowns.create(2550, 270, 'clown');
        this.clown2 = this.game.add.sprite (2550,270,"clown");
        this.game.physics.enable(this.clown2);
        this.game.physics.arcade.enable(this.clown2);
        this.clown2.enableBody = true;
        this.clown2.animations.add('walk', [0, 1, 2], 10, true);
        this.clown2.animations.add('idle', [2], 20, true);
        this.clown2.anchor.setTo (0.5,0.5);	

        //clown3 = this.clowns.create(3648, 100, 'clown');
        this.clown3 = this.game.add.sprite (3648,100,"clown");
        this.game.physics.enable(this.clown3);
        this.game.physics.arcade.enable(this.clown3);
        this.clown3.enableBody = true;
        this.clown3.animations.add('walk', [0, 1, 2], 10, true);
        this.clown3.animations.add('idle', [2], 20, true);
        this.clown3.anchor.setTo (0.5,0.5);

        //clown4 = this.clowns.create(4480, 400, 'clown');
        this.clown4 = this.game.add.sprite (4544,200,"clown");
        this.game.physics.enable(this.clown4);
        this.game.physics.arcade.enable(this.clown4);
        this.clown4.enableBody = true;
        this.clown4.animations.add('walk', [0, 1, 2], 10, true);
        this.clown4.animations.add('idle', [2], 20, true);
        this.clown4.anchor.setTo (0.5,0.5);

        //clown5 = this.clowns.create(6656, 300, 'clown');
        this.clown5 = this.game.add.sprite (6656,300,"clown");
        this.game.physics.enable(this.clown5);
        this.game.physics.arcade.enable(this.clown5);
        this.clown5.enableBody = true;
        this.clown5.animations.add('walk', [0, 1, 2], 10, true);
        this.clown5.animations.add('idle', [2], 20, true);
        this.clown5.anchor.setTo (0.5,0.5);

        //clown6 = this.clowns.create(7040, 200, 'clown');
        this.clown6 = this.game.add.sprite (7040,200,"clown");
        this.game.physics.enable(this.clown6);
        this.game.physics.arcade.enable(this.clown6);
        this.clown6.enableBody = true;
        this.clown6.animations.add('walk', [0, 1, 2], 10, true);
        this.clown6.animations.add('idle', [2], 20, true);
        this.clown6.anchor.setTo (0.5,0.5);

        //clown7 = this.clowns.create(7168, 200, 'clown');
        this.clown7 = this.game.add.sprite (7232,200,"clown");
        this.game.physics.enable(this.clown7);
        this.game.physics.arcade.enable(this.clown7);
        this.clown7.enableBody = true;
        this.clown7.animations.add('walk', [0, 1, 2], 10, true);
        this.clown7.animations.add('idle', [2], 20, true);
        this.clown7.anchor.setTo (0.5,0.5);

        //clown8 = this.clowns.create(9792, 200, 'clown');
        this.clown8 = this.game.add.sprite (9280,300,"clown");
        this.game.physics.enable(this.clown8);
        this.game.physics.arcade.enable(this.clown8);
        this.clown8.enableBody = true;
        this.clown8.animations.add('walk', [0, 1, 2], 10, true);
        this.clown8.animations.add('idle', [2], 20, true);
        this.clown8.anchor.setTo (0.5,0.5);

        //clown9 = this.clowns.create(9792, 0, 'clown');
        this.clown9 = this.game.add.sprite (10048,32,"clown");
        this.game.physics.enable(this.clown9);
        this.game.physics.arcade.enable(this.clown9);
        this.clown9.enableBody = true;
        this.clown9.animations.add('walk', [0, 1, 2], 10, true);
        this.clown9.animations.add('idle', [2], 20, true);
        this.clown9.anchor.setTo (0.5,0.5);
        
        //clown10 = this.clowns.create(117765, 200, 'clown');
        this.clown10 = this.game.add.sprite (11776,200,"clown");
        this.game.physics.enable(this.clown10);
        this.game.physics.arcade.enable(this.clown10);
        this.clown10.enableBody = true;
        this.clown10.animations.add('walk', [0, 1, 2], 10, true);
        this.clown10.animations.add('idle', [2], 20, true);
        this.clown10.anchor.setTo (0.5,0.5);
        
        this.portal = this.game.add.sprite(12288, 300, 'portal');
        this.game.physics.enable(this.portal);
        this.portal.body.gravity = 0;

        //Criando o player
        this.player = this.game.add.sprite(100, 450, 'dude');
        this.game.physics.arcade.enable(this.player);

        this.player.body.gravity.y = 1100;
        this.player.body.collideWorldBounds = true;
        this.game.physics.enable(this.player);

        this.player.animations.add('walk', [1, 2, 3, 4, 5], 10, true);
        this.player.animations.add('idle', [0], 20, true);
        this.player.animations.add('jump', [6], 2, true);
        this.player.anchor.setTo (0.5, 0.5);
        game.camera.follow(this.player);
        
        cursors = game.input.keyboard.createCursorKeys();
        this.jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        pauseBtn = game.input.keyboard.addKey(Phaser.Keyboard.P); // Tela de Pause
        pauseBtn.onDown.add(function(){ 
            game.paused = !game.paused;
            if(game.paused) {
                pause = this.game.add.sprite(this.camera.x, 0, 'pause');
                
            }else {
                pause.visible = false;
            }
        }, this);     
    },

    update: function(){
        game.physics.arcade.collide(this.player, layer);
        game.physics.arcade.collide(this.player, platforms);
                
        //movimentação das bolas
        //bola1
        if (this.bola1.y == 200) {
            game.add.tween(this.bola1).to( { y: 450 }, 500,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola1.y == 450){
        game.add.tween(this.bola1).to( { y: 200 }, 500,Phaser.Easing.Linear.Out, true);	
        }
        //bola2
        if (this.bola2.y == 200) {
            game.add.tween(this.bola2).to( { y: 450 }, 500,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola2.y == 450){
        game.add.tween(this.bola2).to( { y: 200 }, 500,Phaser.Easing.Linear.Out, true);	
        }
        //bola3
        if (this.bola3.y == 64) {
            game.add.tween(this.bola3).to( { y: 200 }, 550,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola3.y == 200){
        game.add.tween(this.bola3).to( { y: 64 }, 850,Phaser.Easing.Linear.Out, true);	
        }
        //bola4
        if (this.bola4.x == 8064) {
            game.add.tween(this.bola4).to( { x: 9152 }, 2500,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola4.x == 9152){
        game.add.tween(this.bola4).to( { x: 8064 }, 2500,Phaser.Easing.Linear.Out, true);	
        }
        //bola5
        if (this.bola5.y == 0) {
            game.add.tween(this.bola5).to( { y: 192 }, 700,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola5.y == 192){
        game.add.tween(this.bola5).to( { y: 0 }, 800,Phaser.Easing.Linear.Out, true);	
        }
        //bola6
        if (this.bola6.y == 192) {
            game.add.tween(this.bola6).to( { y: 0 }, 500,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola6.y == 0){
        game.add.tween(this.bola6).to( { y: 192 }, 400,Phaser.Easing.Linear.Out, true);	
        }
        //bola7
        if (this.bola7.y == 0) {
            game.add.tween(this.bola7).to( { y: 192 }, 600,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola7.y == 192){
        game.add.tween(this.bola7).to( { y: 0 }, 400,Phaser.Easing.Linear.Out, true);	
        }

        //bola8
        if (this.bola8.x == 1664) {
            game.add.tween(this.bola8).to( { x: 1920 }, 450,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola8.x == 1920){
        game.add.tween(this.bola8).to( { x: 1664}, 800,Phaser.Easing.Linear.Out, true);	
        }

        //bola9
        if (this.bola9.x == 2880) {
            game.add.tween(this.bola9).to( { x: 3200 }, 1000,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola9.x == 3200){
        game.add.tween(this.bola9).to( { x: 2880}, 600,Phaser.Easing.Linear.Out, true);	
        }

        //bola10
        if (this.bola10.y == 512) {
            game.add.tween(this.bola10).to( { y: 192 }, 600,Phaser.Easing.Linear.Out, true);
        }
        else if (this.bola10.y == 192){
        game.add.tween(this.bola10).to( { y: 512 }, 400,Phaser.Easing.Linear.Out, true);	
        }

        //bola11
        if (this.distance(this.player.x,this.player.y,this.bola11.x,this.bola11.y) < 500){
            if (this.s11 > 0.3) 
            {
                game.add.tween(this.bola11).to( { y: 320 }, 200,Phaser.Easing.Linear.Out, true);
            }
        }

        //bola12
        if (this.distance(this.player.x,this.player.y,this.bola12.x,this.bola12.y) < 500){
            if (this.s12 > 0.3) 
            {
                game.add.tween(this.bola12).to( { y: 448 }, 200,Phaser.Easing.Linear.Out, true);
            }
        }

        //bola13
        if (this.distance(this.player.x,this.player.y,this.bola13.x,this.bola13.y) < 500){
            if (this.s13 > 0.3) 
            {
                game.add.tween(this.bola12).to( { y: 448 }, 200,Phaser.Easing.Linear.Out, true);
            }
        }

        //bola14
        if (this.distance(this.player.x,this.player.y,this.bola14.x,this.bola14.y) < 500){
            if (this.s14 > 0.3) 
            {
                game.add.tween(this.bola11).to( { y: 320 }, 200,Phaser.Easing.Linear.Out, true);
            }
        }

        bg.tilePosition.x -= 2.3;

        //Colisão dos palhaços
        this.game.physics.arcade.collide(this.clown1, layer);
        this.game.physics.arcade.collide(this.player,this.clown1,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown2, layer);
        this.game.physics.arcade.collide(this.player,this.clown2,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown3, layer);
        this.game.physics.arcade.collide(this.player,this.clown3,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown4, layer);
        this.game.physics.arcade.collide(this.player,this.clown4,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown5, layer);
        this.game.physics.arcade.collide(this.player,this.clown5,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown6, layer);
        this.game.physics.arcade.collide(this.player,this.clown6,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown7, layer);
        this.game.physics.arcade.collide(this.player,this.clown7,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown8, layer);
        this.game.physics.arcade.collide(this.player,this.clown8,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown9, layer);
        this.game.physics.arcade.collide(this.player,this.clown9,this.colisao,null,this);
        this.game.physics.arcade.collide(this.clown10, layer);
        this.game.physics.arcade.collide(this.player,this.clown10,this.colisao,null,this); 
        
        //colisão com as bolas
        this.game.physics.arcade.overlap(this.player, this.bola1,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola2,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola3,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola4,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola5,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola6,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola7,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola8,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola9,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola10,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola11,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola12,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola13,this.colisao,null,this);
        this.game.physics.arcade.overlap(this.player, this.bola14,this.colisao,null,this); 
        
        this.game.physics.arcade.overlap(this.player, this.portal,this.victory,null,this);

        this.player.body.velocity.x = 0;

        if (cursors.left.isDown)
        {
            this.player.body.velocity.x = -250;
            this.player.animations.play('walk');
            if (this.player.scale.x==1) this.player.scale.x = -1;
        }
        else if (cursors.right.isDown)
        {
            this.player.body.velocity.x = 250;
            this.player.animations.play('walk');
            if (this.player.scale.x==-1) this.player.scale.x = 1;	
        }
        else {
            this.player.body.velocity.x = 0;
            this.player.animations.play('idle');

        }
        if ((this.jumpButton.isDown) && (this.player.body.onFloor() || this.player.body.touching.down)) {
            this.player.body.velocity.y =-650;
        } 
        if (!this.player.body.onFloor()) {
            this.player.animations.play('jump');
        }   
        
        if (this.player.y >= 590) {
            this.colisao();
        }
        
        //mecanica do palhaço

        //clown1
        if (this.distance(this.player.x,this.player.y,this.clown1.x,this.clown1.y) < 300){
                this.game.physics.arcade.moveToObject(this.clown1, {y:this.clown1.y+250,x: this.player.x},400,600, this);
                this.setAnimeClown (this.clown1);
        } else {
            this.clown1.body.velocity.x = 0;
            this.clown1.immovable = true;
        }	

        //clown2
        if (this.distance(this.player.x,this.player.y,this.clown2.x,this.clown2.y) < 400){
                this.game.physics.arcade.moveToObject(this.clown2, {y:this.clown2.y+250,x: this.player.x},500,600, this);
        } else {
            //this.clown1.body.velocity.x = 0;
            this.clown1.immovable = true;
        }	

        //clown3
        if (this.distance(this.player.x,this.player.y,this.clown3.x,this.clown3.y) < 250){
                this.game.physics.arcade.moveToObject(this.clown3, {y:this.clown3.y+250,x: this.player.x},700,700, this);
        } else {
            this.clown3.immovable = true;
            //this.clown3.body.velocity.x = 0
        }

            //clown4
        if (this.distance(this.player.x,this.player.y,this.clown4.x,this.clown4.y) < 200){
                this.game.physics.arcade.moveToObject(this.clown4, {y:this.clown4.y+250,x: this.player.x},400,600, this);

            } else {
                this.clown4.immovable = true;
            }

        //clown5
        if (this.distance(this.player.x,this.player.y,this.clown5.x,this.clown5.y) < 175){
                this.game.physics.arcade.moveToObject(this.clown5, {y:this.clown5.y+250,x: this.player.x},400,500, this);

            } else {
                this.clown5.immovable = true;
            }

        //clown6
        if (this.distance(this.player.x,this.player.y,this.clown6.x,this.clown6.y) < 300){
                this.game.physics.arcade.moveToObject(this.clown6, {y:this.clown6.y+250,x: this.player.x},350,400, this);

            } else {
                this.clown6.immovable = true;
            }

            //clown7
            if (this.distance(this.player.x,this.player.y,this.clown7.x,this.clown7.y) < 150){
                this.game.physics.arcade.moveToObject(this.clown7, {y:this.clown7.y+250,x: this.player.x},400,500, this);

            } else {
                this.clown7.immovable = true;
            }	

            //clown8
            if (this.distance(this.player.x,this.player.y,this.clown8.x,this.clown8.y) < 300){
                this.game.physics.arcade.moveToObject(this.clown8, {y:this.clown8.y+250,x: this.player.x},500,500, this);

            } else {
                this.clown8.immovable = true;
            }

            //clown9
            if (Math.abs(this.clown9.x - this.player.x) < 300){
                this.game.physics.arcade.moveToObject(this.clown9, {y:this.clown9.y+250,x: this.player.x},350,400, this);

            } else {
                this.clown9.immovable = true;
                //this.clown9.body.velocity.x = 0
            }
            //clown10
            if (Math.abs(this.clown10.x - this.player.x) < 300){
                this.game.physics.arcade.moveToObject(this.clown10, {y:this.clown10.y+250,x: this.player.x},400,400, this);

            } else {
                this.clown10.immovable = true;
            }
        
        //Animação dos palhaços
        this.setAnimeClown (this.clown1);
        this.setAnimeClown (this.clown2);
        this.setAnimeClown (this.clown3);
        this.setAnimeClown (this.clown4);
        this.setAnimeClown (this.clown5);
        this.setAnimeClown (this.clown6);
        this.setAnimeClown (this.clown7);
        this.setAnimeClown (this.clown8);
        this.setAnimeClown (this.clown9);
        this.setAnimeClown (this.clown10);
    },

    colisao: function (player, objeto) {
        this.player.kill();
        audio.destroy(); 
        this.game.state.start('dead'); 
    },
    
    victory: function() {
        this.player.kill();
        audio.destroy();
        this.game.state.start('win');
    },

    distance: function (x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return Math.abs(dx + dy);
    },

    setAnimeClown: function (clown){
        if (clown.body.velocity.x == 0) {
            clown.animations.play('idle');

        }
        else if (clown.body.velocity.x < 0)
        {
            clown.animations.play('walk');
            if (clown.scale.x==1) clown.scale.x = -1;
        }
        else if (clown.body.x > 0)
        {
            clown.animations.play('walk');
            if (clown.scale.x==-1) clown.scale.x = 1;	
        }
    }
    }