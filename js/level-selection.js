"use strict"; 

var transition;
var platforms;
var jumpButton;
var music;
var cursors;
var player;

var phaseSelectState = {
    
    preload: function() {
        this.game.load.image('background', 'assets/backgrounds/backgroundPhaseSelection.jpg'); // Plano de fundo
        this.game.load.spritesheet('dude', 'assets/player/player.png', 65, 84);  // Personagem principal
        this.game.load.image('platform', 'assets/platform.jpg');            // Plataforma sobre a qual o personagem está
        this.game.load.spritesheet('door', 'assets/buttons/doorButton.png', 183, 367); // 'Portal' para a primeira fase 
        this.game.load.image('mist', 'assets/backgrounds/mist.png'); // Nevoeiro
        this.game.load.spritesheet('transition', 'assets/transitions/phaseTransition.png', 1024, 600); // Splash
        this.game.load.audio('audio', 'assets/audio/level_selection.ogg'); // Audio
    }, 
    
    goToGame: function(){   //Função que redireciona para o jogo (fase 01)     
        this.game.state.start('game');
    },
    
    playAnimation: function() {
        music.mute = true;
        
        transition = game.add.sprite(0, 0, 'transition');
        transition.animations.add('go', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 2000, true);
        transition.animations.play('go');
        
        this.game.time.events.add(900, this.goToGame, this); 
    },
    
    create: function(){
        music = game.add.audio('audio', 1, true);
        music.play();
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
    
        this.game.add.tileSprite(0, 0, game.width, game.height, 'background');
                        
        player = this.game.add.sprite(100, 450, 'dude');
        game.physics.arcade.enable(player);

        player.body.gravity.y = 1100;
        player.body.collideWorldBounds = true;
        game.physics.enable(player);

        player.animations.add('walk', [1, 2, 3, 4, 5], 10, true);
        player.animations.add('idle', [0], 20, true);
        player.animations.add('jump', [6], 2, true);
             
        platforms = game.add.physicsGroup();
        platforms.create(0, 530, 'platform');
        platforms.setAll('body.immovable', true);
                
        cursors = game.input.keyboard.createCursorKeys();
        jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        this.game.add.button(450, 0, 'door', this.playAnimation.bind(this), this, 1, 2); 
        this.game.add.tileSprite(0, 0, game.width, game.height, 'mist');
    },
    
   update: function(){
        game.physics.arcade.collide(player, layer);
        game.physics.arcade.collide(player, platforms);

        player.body.velocity.x = 0;

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
            player.animations.play('walk');
            if (player.scale.x == 1) player.scale.x = -1;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
            player.animations.play('walk');
            if (player.scale.x == -1) player.scale.x = 1;	
        }
        else {
            player.body.velocity.x = 0;
            player.animations.play('idle');
        }
        if ((jumpButton.isDown) && (player.body.onFloor() || player.body.touching.down)) {
            player.body.velocity.y =- 650;

        } 
        if (!player.body.touching.down) {
            player.animations.play('jump');
        }  
    }
}
